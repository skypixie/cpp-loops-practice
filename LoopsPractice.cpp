﻿#include <iostream>


// printEven: true - печатать четные, false - нечетные
void printNumbers(int Limit, bool printEven)
{
	// если печатаем четные, то надо начинать отсчет с 0, иначе с 1
	int number = printEven ? 0 : 1;

	for (; number <= Limit; number += 2)
	{
		std::cout << number << '\n';
	}
}


int main()
{
	int Limit{ 20 };

	for (int i = 0; i <= Limit; i += 2)
	{
		std::cout << i << '\n';
	}

	// напечатаем четные
	printNumbers(20, true);

	// напечатаем нечетные
	printNumbers(20, false);
}
